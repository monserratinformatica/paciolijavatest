package com.monserrat;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.fasterxml.jackson.databind.PropertyNamingStrategy;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpPost;
import org.apache.http.entity.ContentType;
import org.apache.http.entity.StringEntity;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClientBuilder;

import java.io.IOException;

public class Pacioli {
    // Retorna folio de factura si estaba ok
    public int EnviarFactura(Factura factura) throws IOException {
        int result = -1;
        ObjectMapper objectMapper = new ObjectMapper();

        CloseableHttpClient httpClient = HttpClientBuilder.create().build();
        try {
            HttpPost request = new HttpPost("http://localhost:28933/api/FacturaElectronica");

            System.out.println(objectMapper.writeValueAsString(factura));

            StringEntity params = new StringEntity(objectMapper.writeValueAsString(factura), ContentType.APPLICATION_JSON);
            request.addHeader("content-type", "application/json");
            request.setEntity(params);
            CloseableHttpResponse response = httpClient.execute(request);
            if (response.getStatusLine().getStatusCode() == 200) {
                objectMapper.setPropertyNamingStrategy(new PropertyNamingStrategy.PascalCaseStrategy());
                ResultadoEnvio resultadoEnvio = objectMapper.readValue(response.getEntity().getContent(), ResultadoEnvio.class);
                result = resultadoEnvio.getFolio();
            } else {
                System.out.println("Ocurrió un error enviando el documento");
            }
        } finally {
            httpClient.close();
        }
        return result;
    }


}
