package com.monserrat;

import java.io.IOException;

public class PacioliTest {

    public static void main(String[] args) {
        Detalle detalle = new Detalle();
        detalle.setCantidad(1);
        detalle.setPrecio(100);
        detalle.setNombreItem("Factura desde Java");
        detalle.setMonto(detalle.getCantidad() * detalle.getPrecio());
        detalle.setPorcentajeDescuento(0);
        detalle.setExento(false);

        Empresa empresa = new Empresa();
        empresa.setRazonSocial("Empresa de Prueba");
        empresa.setRUT(1234567);
        empresa.setGiro("Giro de Prueba");
        empresa.setDireccion("Dirección de prueba");
        empresa.setComuna("Huechuraba");
        empresa.setCiudad("Santiago");

        Factura factura = new Factura();
        factura.setFormaDePago("Al contado");
        factura.setReceptor(empresa);
        factura.getDetalle().add(detalle);

        Pacioli servicio = new Pacioli();
        try {
            int nuevoFolio = servicio.EnviarFactura(factura);
            System.out.println("Factura enviada exitosamente - folio obtenido: " + nuevoFolio);
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
    }
}
