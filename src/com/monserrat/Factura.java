package com.monserrat;

import java.util.ArrayList;
import java.util.List;

public class Factura {
    String FormaDePago;
    List<Detalle> Detalle = new ArrayList<Detalle>();
    Empresa Receptor;

    public Empresa getReceptor() {
        return Receptor;
    }

    public void setReceptor(Empresa receptor) {
        Receptor = receptor;
    }

    public String getFormaDePago() {
        return FormaDePago;
    }

    public void setFormaDePago(String formaDePago) {
        FormaDePago = formaDePago;
    }

    public List<Detalle> getDetalle() {
        return Detalle;
    }

    public void setDetalle(List<Detalle> detalle) {
        Detalle = detalle;
    }
}
