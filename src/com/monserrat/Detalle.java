package com.monserrat;

public class Detalle {
    int Cantidad;
    boolean Exento;
    long Monto;
    String NombreItem;
    float PorcentajeDescuento;
    long Precio;
    String TipoCodigo;
    String ValorCodigo;
    String UnidadMedida;

    public int getCantidad() {
        return Cantidad;
    }

    public void setCantidad(int cantidad) {
        Cantidad = cantidad;
    }

    public boolean isExento() {
        return Exento;
    }

    public void setExento(boolean exento) {
        Exento = exento;
    }

    public long getMonto() {
        return Monto;
    }

    public void setMonto(long monto) {
        Monto = monto;
    }

    public String getNombreItem() {
        return NombreItem;
    }

    public void setNombreItem(String nombreItem) {
        NombreItem = nombreItem;
    }

    public float getPorcentajeDescuento() {
        return PorcentajeDescuento;
    }

    public void setPorcentajeDescuento(float porcentajeDescuento) {
        PorcentajeDescuento = porcentajeDescuento;
    }

    public long getPrecio() {
        return Precio;
    }

    public void setPrecio(long precio) {
        Precio = precio;
    }

    public String getTipoCodigo() {
        return TipoCodigo;
    }

    public void setTipoCodigo(String tipoCodigo) {
        TipoCodigo = tipoCodigo;
    }

    public String getValorCodigo() {
        return ValorCodigo;
    }

    public void setValorCodigo(String valorCodigo) {
        ValorCodigo = valorCodigo;
    }

    public String getUnidadMedida() {
        return UnidadMedida;
    }

    public void setUnidadMedida(String unidadMedida) {
        UnidadMedida = unidadMedida;
    }
}
