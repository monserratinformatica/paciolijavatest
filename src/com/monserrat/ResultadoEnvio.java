package com.monserrat;

/**
 * Created by Leonardo on 13-03-2015.
 */
public class ResultadoEnvio {
    int Id;
    int Folio;

    public int getId() {
        return Id;
    }

    public void setId(int id) {
        Id = id;
    }

    public int getFolio() {
        return Folio;
    }

    public void setFolio(int folio) {
        Folio = folio;
    }
}
